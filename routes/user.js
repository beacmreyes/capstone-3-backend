const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')
const moment = require('moment')

//Check if email exists
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

//User Registration
router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})
module.exports = router

//Login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//for user details and records
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.getPrivate({ userId: user.id }).then(user => res.send(user))
})

router.put('/setDate', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        startDate: req.body.startDate,
        endDate: req.body.endDate
    }
    UserController.setDate(params).then(result => res.send(result))
})





router.post('/duplicateCategory', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        category: {
        	type: req.body.type,
        	name:req.body.name,
        }
    }
    UserController.duplicateCategory(params).then(result => res.send(result))
})

router.put('/addCategory', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        category: {
            type: req.body.type,
            name:req.body.name,
            budget:req.body.budget
        }
    }
    UserController.addCategory(params).then(result => res.send(result))
})

router.put('/editCategory', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        category: {
            categoryId:req.body.categoryId,
            type: req.body.type,
            name:req.body.name,
            budget:req.body.budget
        }
    }
    UserController.editCategory(params).then(result => res.send(result))
})

router.put('/deleteCategory', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        categoryId:req.body.categoryId
    }
    UserController.deleteCategory(params).then(result => res.send(result))
})



router.put('/addRecord', auth.verify, (req, res) => {
    const params = {
        userId: auth.decode(req.headers.authorization).id,
        record:{
            categoryId: req.body.categoryId,
            description:req.body.description,
            amount:req.body.amount,
            updatedOn:moment(req.body.updatedOn)            
        }
    }
    UserController.addRecord(params).then(result => res.send(result))
})


// Verify Google Token
router.post('/verify-google-id-token', async (req, res)=>{
    res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})


module.exports = router
