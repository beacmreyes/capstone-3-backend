const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = "705089561550-sh5j061fh45spu9s4gpg2noov5e14qjv.apps.googleusercontent.com"
const moment = require('moment')

const errCatcher = err => console.log(err)

module.exports.emailExists = (params) => {
    return User.find({ email: params.email })
    .then(result => {
		return result.length > 0 ? true : false
    })
    .catch(errCatcher)
}

module.exports.register = (params) => {
	let user = new User({
        firstName: params.firstName,
        lastName: params.lastName,
        email: params.email,
		password: bcrypt.hashSync(params.password, 10),
        loginType:'email',
        currentDate:{
            startDate:moment().startOf('month'),
            endDate:moment().endOf('month')
        }
	})


    return user.save()
    .then((newUser, err) => {
		return (err) ? false : true
    })
    .catch(errCatcher)
}



module.exports.login = (params) => {
    return User.findOne({email: params.email}).then(resultFromFindOne => {
        if(resultFromFindOne === null){ //user doesn't exist
            return {
                error: 'does-not-exist'
            }
        }
        if (resultFromFindOne.loginType !=='email'){
            return {
                error: 'login-type-error'
            }
        }

        const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password) //decrypts password

        if(isPasswordMatched){
            return {
                accessToken: auth.createAccessToken(resultFromFindOne.toObject()),
                userId:resultFromFindOne._id
            }
        } else {
            return {error: 'incorrect-password'}
        }
    })
}

module.exports.getPrivate = (params) => {
    return User.findById(params.userId)
    .then(user => {
        user.password = undefined
        return user
    })
    .catch(errCatcher)
}

module.exports.setDate = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){
            return "error"
        }
        user.currentDate.startDate=moment(params.startDate)
        user.currentDate.endDate=moment(params.endDate)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : updatedUser.currentDate
         })
    })
}




//add category
// module.exports.addCategory = (params) => {
//     return User.findById(params.userId)
//     .then((user, err) => {
//         if(err){
//             return "error"
//         }
//         if(user.categories.length>0){
//             console.log(1)
//             return user.categories.map(category => {
//                 if (category.name.toLowerCase()==params.category.name.toLowerCase() && category.type.toLowerCase()==params.category.type.toLowerCase()){
//                     console.log(2)
//                     return 'duplicate-found'
//                 }else{
//                     console.log(3)
//                     user.categories.push(params.category)
//                     return user.save()
//                     .then((updatedUser, err) => {
//                         return err ? false : true
//                     })
//                 }
//             })

//         }else{
//             user.categories.push(params.category)
//             return user.save()
//             .then((updatedUser, err) => {
//                 return err ? false : true
//             })              
//         }

//     })

// }

module.exports.duplicateCategory = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){
            return "error"
        }

        const duplicate=user.categories.find(category => category.name.toLowerCase()==params.category.name.toLowerCase() && category.type.toLowerCase()==params.category.type.toLowerCase())
        return duplicate? true:false
        
    })
}

module.exports.addCategory = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){
            return "error"
        }
        user.categories.push(params.category)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
         })
    })
}




//edit category
module.exports.editCategory = (params) => {
    console.log(2)
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){ return "error"}
        console.log(1)
        categoryIndex=user.categories.findIndex(category => category._id==params.category.categoryId)
        user.categories[categoryIndex].type=params.category.type
        user.categories[categoryIndex].name=params.category.name
        user.categories[categoryIndex].budget=params.category.budget

        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
        })
        .catch(errCatcher)

    })
    .catch(errCatcher)
}


//delete Category
module.exports.deleteCategory = (params) => {
    console.log(2)
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){ return "error"}
        console.log(1)
        categoryIndex=user.categories.findIndex(category => category._id==params.category.categoryId)

        if(categoryIndex>-1){
            user = user.splice(categoryIndex, 1)
        }

        return user.save()
        .then((updatedUser, err) => {
            return err ? false : updatedUser
        })
        .catch(errCatcher)

    })
    .catch(errCatcher)
}



module.exports.addRecord = (params) => {
    return User.findById(params.userId)
    .then((user, err) => {
        if(err){
            return "error"
        }
        console.log(user)
        console.log(params.record)
        user.records.push(params.record)
        return user.save()
        .then((updatedUser, err) => {
            return err ? false : true
         })
    })
}


module.exports.verifyGoogleTokenId = async (tokenId) =>{

    const client = new OAuth2Client(clientId)
    const data = await client.verifyIdToken({
        idToken: tokenId,
        audience: clientId
    })


    console.log(data.payload)

    if(data.payload.email_verified === true){
        const user = await User.findOne({ email: data.payload.email}).exec();

        if (user !== null){
            if(user.loginType === "google"){
                return{ accessToken: auth.createAccessToken(user.toObject())}
            }else{
                return {error: "login-type-error"}
            }
        } else {
            const newUser = new User({
                firstName: data.payload.given_name,
                lastName: data.payload.family_name,
                email:data.payload.email,
                loginType:"google",
                currentDate:{
                    startDate:moment().startOf('day'),
                    endDate:moment().add('days', 30).startOf('day')
                }
            })

            return newUser.save().then((user,err) =>{
                return {accessToken: auth.createAccessToken(user.toObject())}
            })
        }
    } else {
        return {error: "google-auth-error"}
    }
}