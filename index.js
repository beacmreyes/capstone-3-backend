const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv').config()
const cors = require('cors')
const port = process.env.PORT
const mongodbCloud = process.env.DB_MONGODB
console.log(port)


const userRoutes = require('./routes/user')

mongoose.connection.once('open', () => console.log('Now connected to MongoDB cloud.'))
mongoose.connect('mongodb+srv://admin:admin123@cluster0.p12ay.mongodb.net/budgettracker?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

app.use(express.json())
app.use(express.urlencoded({ extended: true }))



// const corsOptions = {
// 	origin: 'http://localhost:3000/',
// 	optionsSuccessStatus: 200//for compatibility with older browsers
// }

// app.use(cors(corsOptions));


module.exports = {
  async headers() {
    return [
      {
        // matching all API routes
        source: "/api/:path*",
        headers: [
          { key: "Access-Control-Allow-Credentials", value: "true" },
          { key: "Access-Control-Allow-Origin", value: "*" },
          { key: "Access-Control-Allow-Methods", value: "GET,OPTIONS,PATCH,DELETE,POST,PUT" },
          { key: "Access-Control-Allow-Headers", value: "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version" },
        ]
      }
    ]
  }
};

app.use(cors());
app.options("*", cors())


app.use('/api/users', userRoutes)

app.listen(port, () => {
    console.log(`API is now online on port ${ port }`)
})



