const mongoose = require('mongoose')


const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First Name is required.']
    },
    lastName: {
        type: String,
        required: [true, 'Last Name is required.']
    },    

    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    password: {
        type: String
    },
    loginType:{
        type: String,
        required: [true, 'Login type is required.']
    },
    currentDate:{
        startDate:{
            type: Date,
            required: [true, 'Start Date is required']
        },
        endDate:{
            type:Date,
            required: [true, 'End Date is required']
        }
    },
    categories:[
    	{
	        type:{
                type: String,
                required: [true, 'Category type is required']
            },
            name:{
                type:String,
                required: [true, 'Category name is required']
            },
            budget:{
                type: Number,
                default: 0
            }

    	}
    ],    
    records: [
       {
            categoryId:{
                type:String,
                // required: [true, 'Category name is required']
            },
            description:{
                type:String,
                required: [true, 'Description is required']
            },
            amount:{
                type: Number,
                required: [true, 'Amount is required']
            },
            updatedOn:{
                type: Date,
                required: [true, 'Date is required']
            }
        }
    ]

})

module.exports = mongoose.model('user', userSchema)